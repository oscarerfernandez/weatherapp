import { Injectable } from '@angular/core';
import { CityModel } from '../../model';
import { MyError } from './my-error';
import { HttpClient } from '@angular/common/http';
import { BaseHttpService } from './base.http.service';
import { NzModalService } from 'ng-zorro-antd';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import cities from '../../../../assets/city.list.json';

@Injectable({ providedIn: 'root' })
export class CityService extends BaseHttpService<CityModel> {
    constructor(
        public http: HttpClient,
        public myErr: MyError,
        public modalService: NzModalService) {
        super(http, myErr, `http://api.openweathermap.org/data/2.5/weather?appid=${environment.apiKey}`, modalService);
    }

    public pepeObservable:string = "string antes";

    getCityList(): Promise<Array<CityModel>> {
        return new Promise((resolve, reject) => resolve(cities));
    }

}
