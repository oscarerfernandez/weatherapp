export interface Id {
    id?: number;
}
export interface ICoords {
    lat: number;
    lon: number;
}

export interface IWeather {
    id: number;
    main: string;
    description: string;
    icon: string;
    lat: number;
    lon: number;
}

export interface IMain {
    temp: number;
    feels_like: number;
    temp_min: number;
    temp_max: number;
    pressure: number;
    humidity: number
}

export interface IWind {
    speed: number;
    deg: number;
}

export interface IClouds {
    all: number;
}

export interface ISys {
    type: number;
    id: number;
    country: string;
    sunrise: number;
    sunset: number;
}