import { Id, ICoords, IWeather, IMain, IWind, IClouds, ISys } from './common.model';

export class WeatherDataModel implements Id {
    public id?: number;
    public timezone: number;
    public cod: number;
    public base: string;
    public visibility: number;
    public dt: number;
    public weather: Array<IWeather>;
    public main: IMain;
    public win: IWind;
    public sys: ISys;
    public clouds: IClouds;
    public country: string;
    public name: string;
    public state: string;
    public coord: ICoords;

    constructor(model: any = {}) {
        this.id = model.id;
        this.country = model.country;
        this.name = model.name;
        this.state = model.state;
        this.coord = this.coord;
        this.timezone = this.timezone;
        this.cod = this.cod;
        this.base = this.base;
        this.visibility = this.visibility;
        this.dt = this.dt;
        this.weather = this.weather;
        this.main = this.main;
        this.win = this.win;
        this.sys = this.sys;
    }
}