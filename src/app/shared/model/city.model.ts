import { Id, ICoords } from './common.model';

export class CityModel implements Id {
    public id?: number;
    public country: string;
    public name: string;
    public state: string;
    public coord: ICoords;

    constructor(model: any = {}) {
        this.id = model.id;
        this.country = model.country;
        this.name = model.name;
        this.state = model.state;
        this.coord.lat = this.coord.lat;
        this.coord.lon = this.coord.lon;
    }
}
