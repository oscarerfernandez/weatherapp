import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from './header.component';
import {HeaderIconComponent} from './header-icon/header-icon.component';
import {SharedModule} from '../../shared/shared.module';
import {RouterModule} from '@angular/router';


@NgModule({
  declarations: [
    HeaderComponent,
    HeaderIconComponent],
  exports: [
    HeaderComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule
  ]
})
export class HeaderModule {
}
