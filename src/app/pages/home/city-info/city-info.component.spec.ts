import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CityInfoWeatherComponent } from './city-info.component';

describe('CityInfoWeatherComponent', () => {
  let component: CityInfoWeatherComponent;
  let fixture: ComponentFixture<CityInfoWeatherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CityInfoWeatherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CityInfoWeatherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
