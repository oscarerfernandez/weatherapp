import { Component, Input, OnInit } from '@angular/core';
import { CityService, CityModel, WeatherDataModel } from '../../../shared/';
import { NzModalRef } from 'ng-zorro-antd';

@Component({
    selector: 'app-grupo-info',
    templateUrl: './city-info.component.html',
    styleUrls: ['./city-info.component.scss']
})
export class CityInfoWeatherComponent implements OnInit {
    @Input() data: CityModel;
    @Input() weatherData: Array<WeatherDataModel>;

    isLoading = true;

    constructor(
        private $city: CityService,
        private modal: NzModalRef
    ) {
        this.weatherData = [];
    }

    ngOnInit() {
        console.log('data.id', this.data.id.toString())
        this.$city.getByParam(this.data.id.toString()).subscribe(dataWeather => {
            this.weatherData.push(dataWeather as WeatherDataModel);
            console.log('data', this.weatherData);
            this.isLoading = false;
        });
    }

    onClose() {
        this.modal.destroy(false);
    }

}
