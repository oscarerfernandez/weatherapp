import { Component, OnInit } from '@angular/core';
import { CityService, CityModel } from '../../shared'
import { CityInfoWeatherComponent } from './city-info/city-info.component';
import { NzModalService } from 'ng-zorro-antd';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    loading: boolean = true;
    sortName = '';
    sortValue = '';
    searchValue = '';
    newPepeValue = '';
    listOfData: Array<CityModel> = [];
    listOfDisplayData: Array<CityModel> = [];

    constructor(
        private $city: CityService,
        private mService: NzModalService,
    ) { }

    ngOnInit() {
        this.$city.getCityList().then(cities => {
            this.loading = false;
            this.listOfData = cities;
            this.bindData();            
        });
    }

    bindData() {
        this.listOfDisplayData = this.listOfData.sort(function (a, b) { return 0.5 - Math.random() });
    }

    reset(): void {
        this.searchValue = '';
        this.bindData();
    }

    resetPepeValue(): void {
        this.searchValue = '';
        this.bindData();
    }

    sort(sort: { key: string; value: string }): void {
        this.sortName = sort.key;
        this.sortValue = sort.value;
        this.search();
    }

    onView(data: CityModel) {
        this.mService.create({
            nzTitle: `Clima en ${data.name}`,
            nzContent: CityInfoWeatherComponent,
            nzComponentParams: {
                data
            }
        });
    }

    modificarPepeVariable() {
        console.log(this.newPepeValue)
        this.$city.pepeObservable = this.newPepeValue;
        console.log(this.$city.pepeObservable)
    }

    search() {
        const data = this.listOfData.filter(d => {
            if ((d != null || d != undefined)) {
                return ((d.name) && d.name.toLowerCase().includes(this.searchValue.toLowerCase())) ||
                    ((d.id) && d.id.toLocaleString().toLowerCase().includes(this.searchValue.toLowerCase())) ||
                    ((d.name) && d.name.toLowerCase().includes(this.searchValue.toLowerCase())) ||
                    ((d.country) && d.country.toLowerCase().includes(this.searchValue.toLowerCase())) ||
                    ((d.coord.lat) && d.coord.lat.toLocaleString().toLowerCase().includes(this.searchValue.toLowerCase())) ||
                    ((d.coord.lon) && d.coord.lon.toLocaleString().toLowerCase().includes(this.searchValue.toLowerCase()))
            }
            return false
        });
        this.listOfDisplayData = data.sort((a, b) => {
            if (a[this.sortName] == undefined) {
                return 1
            }
            if (b[this.sortName] == undefined) {
                return -1
            }
            return this.sortValue === 'ascend'
                // tslint:disable-next-line:no-non-null-assertion
                ? a[this.sortName!] <= b[this.sortName!] ? -1 : 1
                // tslint:disable-next-line:no-non-null-assertion
                : b[this.sortName!] > a[this.sortName!]
                    ? 1
                    : -1;
        }
        );
    }
}

