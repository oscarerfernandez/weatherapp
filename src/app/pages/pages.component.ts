import {Component, OnInit} from '@angular/core';
import {NavigationStart, Router} from '@angular/router';
import {DatePipe} from '@angular/common';


@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {
  isCollapsed = false;
  navList = [
    {
      title: 'home', icon: 'home', isOpen: true, child:
        [
            {title: 'inicio', url: '/pages/home'},
            {title: 'pepe', url: '/pages/pepe'}
        ]
    }
  ];

  constructor(private router: Router,
              private datePipe: DatePipe) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
      }
    });
  }

  ngOnInit() {
  }
}
