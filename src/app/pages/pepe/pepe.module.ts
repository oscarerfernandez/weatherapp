import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PepeRoutingModule } from './pepe-routing.module';
import { PepeComponent } from './pepe.component';
import {SharedModule} from '../../shared/shared.module';


@NgModule({
  declarations: [PepeComponent],
  imports: [
    CommonModule,
    PepeRoutingModule,
    SharedModule
  ]
})
export class PepeModule { }
