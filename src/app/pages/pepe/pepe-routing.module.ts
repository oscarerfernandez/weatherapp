import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PepeComponent} from './pepe.component';


const routes: Routes = [{path: '', component: PepeComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PepeRoutingModule {
}
